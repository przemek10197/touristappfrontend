import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home";
import AttractionList from "../views/AttractionList";
import HotelList from "../views/HotelList";
import RestaurantList from "../views/RestaurantList";
import AddObject from "../views/AddObject";
import Login from "../components/User/Login";
import Favourites from "../views/Favourites";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "home",
    component: Home
  },
  {
    path: "/attractions",
    name: "attractions",
    component: AttractionList
  },
  {
    path: "/hotels",
    name: "hotels",
    component: HotelList
  },
  {
    path: "/restaurants",
    name: "restaurants",
    component: RestaurantList
  },
  {
    path: "/addplace",
    name: "addplace",
    component: AddObject
  },
  {
    path: "/login",
    name: "login",
    component: Login
  },
  {
    path: "/favourites",
    name: "favourites",
    component: Favourites
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
