import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    attractionsLimited: [],
    searchedAttractions: {},
    searchedHotels: {},
    searchedRestaurants: {},
    hotelsLimited: [],
    restaurantsLimited: [],
    attractions: [],
    hotels: [],
    restaurants: [],
    cities: [],
    attractionTypes: [],
    hotelTypes: [],
    restaurantTypes: [],
    navbarId: 0,
    favouriteObjects: {},
    addedObject: false,
    deleteSnackbar: false,
    editSnackbar: false
  },
  getters: {
    attractionsLimited: state => {
      return state.attractionsLimited;
    },
    searchedAttractions: state => {
      return state.searchedAttractions;
    },
    searchedHotels: state => {
      return state.searchedHotels;
    },
    searchedRestaurants: state => {
      return state.searchedRestaurants;
    },
    hotelsLimited: state => {
      return state.hotelsLimited;
    },
    restaurantsLimited: state => {
      return state.restaurantsLimited;
    },
    attractions: state => {
      return state.attractions;
    },
    hotels: state => {
      return state.hotels;
    },
    restaurants: state => {
      return state.restaurants;
    },
    cities: state => {
      return state.cities;
    },
    attractionTypes: state => {
      return state.attractionTypes;
    },
    hotelTypes: state => {
      return state.hotelTypes;
    },
    restaurantTypes: state => {
      return state.restaurantTypes;
    },
    navbarId: state => {
      return state.navbarId;
    },
    favouriteObjects: state => {
      return state.favouriteObjects;
    },
    addedObject: state => {
      return state.addedObject;
    },
    deleteSnackbar: state => {
      return state.deleteSnackbar;
    },
    editSnackbar: state => {
      return state.editSnackbar;
    }
  },
  mutations: {
    setLimitedAttractions(state, value) {
      state.attractionsLimited = value.monuments.slice(0, 3);
      state.attractions = value;
    },
    setLimitedHotels(state, value) {
      state.hotelsLimited = value.hotels.slice(0, 3);
      state.hotels = value;
    },
    setLimitedRestaurants(state, value) {
      state.restaurantsLimited = value.restaurants.slice(0, 3);
      state.restaurants = value;
    },
    setCities(state, value) {
      state.cities = value;
    },
    setAttractionTypes(state, value) {
      state.attractionTypes = value;
    },
    setHotelTypes(state, value) {
      state.hotelTypes = value;
    },
    setRestaurantTypes(state, value) {
      state.restaurantTypes = value;
    },
    setNavbarId(state) {
      state.navbarId += 1;
    },
    setSearchedAttractions(state, value) {
      state.searchedAttractions = value;
    },
    setSearchedHotels(state, value) {
      state.searchedHotels = value;
    },
    setSearchedRestaurants(state, value) {
      state.searchedRestaurants = value;
    },
    setFavouriteObjects(state, value) {
      state.favouriteObjects = value;
    },
    setAddedObject(state) {
      state.addedObject = true;
    },
    resetAddedObject(state) {
      state.addedObject = false;
    },
    setDeleteSnackbar(state) {
      state.deleteSnackbar = true;
    },
    resetDeleteSnackbar(state) {
      state.deleteSnackbar = false;
    },
    setEditSnackbar(state) {
      state.editSnackbar = true;
    },
    resetEditSnackbar(state) {
      state.editSnackbar = false;
    }
  },
  actions: {
    loadLimitedAttractions({ commit }) {
      axios
        .get(`${process.env.VUE_APP_URL}/api/Attraction/GetAll`, {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`
          }
        })
        .then(response => {
          commit("setLimitedAttractions", response.data);
        })
        .catch(error => {
          console.log(error);
        });
    },
    loadLimitedHotels({ commit }) {
      axios
        .get(`${process.env.VUE_APP_URL}/api/Hotel/GetAll`, {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`
          }
        })
        .then(response => {
          commit("setLimitedHotels", response.data);
        })
        .catch(error => {
          console.log(error);
        });
    },
    loadLimitedRestaurants({ commit }) {
      axios
        .get(`${process.env.VUE_APP_URL}/api/Restaurant/GetAll`, {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`
          }
        })
        .then(response => {
          commit("setLimitedRestaurants", response.data);
        })
        .catch(error => {
          console.log(error);
        });
    },
    loadSearchedAttractions({ commit }, value) {
      axios
        .get(`${process.env.VUE_APP_URL}/api/Attraction/find/${value}`, {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`
          }
        })
        .then(response => {
          commit("setSearchedAttractions", response.data);
          console.log(response.data);
        })
        .catch(error => {
          console.log(error);
        });
    },
    loadSearchedHotels({ commit }, value) {
      axios
        .get(`${process.env.VUE_APP_URL}/api/Hotel/find/${value}`, {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`
          }
        })
        .then(response => {
          console.log(response.data);
          commit("setSearchedHotels", response.data);
        })
        .catch(error => {
          console.log(error);
        });
    },
    loadSearchedRestaurants({ commit }, value) {
      axios
        .get(`${process.env.VUE_APP_URL}/api/Restaurant/find/${value}`, {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`
          }
        })
        .then(response => {
          console.log(response.data);
          commit("setSearchedRestaurants", response.data);
        })
        .catch(error => {
          console.log(error);
        });
    },
    loadCities({ commit }) {
      axios
        .get(`${process.env.VUE_APP_URL}/api/Cities`, {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`
          }
        })
        .then(response => {
          commit("setCities", response.data);
        })
        .catch(error => {
          console.log(error);
        });
    },
    loadAttractionTypes({ commit }) {
      axios
        .get(`${process.env.VUE_APP_URL}/api/AttractionType`, {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`
          }
        })
        .then(response => {
          commit("setAttractionTypes", response.data);
        })
        .catch(error => {
          console.log(error);
        });
    },
    loadHotelTypes({ commit }) {
      axios
        .get(`${process.env.VUE_APP_URL}/api/HotelType`, {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`
          }
        })
        .then(response => {
          commit("setHotelTypes", response.data);
        })
        .catch(error => {
          console.log(error);
        });
    },
    loadRestaurantTypes({ commit }) {
      axios
        .get(`${process.env.VUE_APP_URL}/api/RestaurantType`, {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`
          }
        })
        .then(response => {
          commit("setRestaurantTypes", response.data);
        })
        .catch(error => {
          console.log(error);
        });
    },
    submitEvent({ commit }, value) {
      const objectToAdd = {
        name: value.name,
        cityName: value.city,
        typeName: value.type,
        address: value.address,
        description: value.description
      };
      if (value.place === 0) {
        axios
          .post(`${process.env.VUE_APP_URL}/api/Attraction/Add`, objectToAdd, {
            headers: {
              Authorization: `Bearer ${localStorage.getItem("token")}`
            }
          })
          .then(response => {
            console.log(response);
            commit("setAddedObject");
          })
          .catch(error => {
            console.log(error);
          });
      } else if (value.place === 1) {
        axios
          .post(`${process.env.VUE_APP_URL}/api/Hotel/Add`, objectToAdd, {
            headers: {
              Authorization: `Bearer ${localStorage.getItem("token")}`
            }
          })
          .then(response => {
            console.log(response);
            commit("setAddedObject");
          })
          .catch(error => {
            console.log(error);
          });
      } else {
        axios
          .post(`${process.env.VUE_APP_URL}/api/Restaurant/Add`, objectToAdd, {
            headers: {
              Authorization: `Bearer ${localStorage.getItem("token")}`
            }
          })
          .then(response => {
            console.log(response);
            commit("setAddedObject");
          })
          .catch(error => {
            console.log(error);
          });
      }
    },
    resetAddedObject({ commit }) {
      commit("resetAddedObject");
    },
    setNavbarId({ commit }) {
      commit("setNavbarId");
    },
    addAttractionToFavourites({ commit }, value) {
      axios
        .post(
          `${process.env.VUE_APP_URL}/api/Favourite/Add?id=${value}&name=monument`,
          [],
          {
            headers: {
              Authorization: `Bearer ${localStorage.getItem("token")}`
            }
          }
        )
        .then(response => {
          commit("setNavbarId");
          console.log(response);
        })
        .catch(error => {
          console.log(error);
        });
    },
    addHotelToFavourites({ commit }, value) {
      axios
        .post(
          `${process.env.VUE_APP_URL}/api/Favourite/Add?id=${value}&name=hotel`,
          [],
          {
            headers: {
              Authorization: `Bearer ${localStorage.getItem("token")}`
            }
          }
        )
        .then(response => {
          commit("setNavbarId");
          console.log(response);
        })
        .catch(error => {
          console.log(error);
        });
    },
    addRestaurantToFavourites({ commit }, value) {
      axios
        .post(
          `${process.env.VUE_APP_URL}/api/Favourite/Add?id=${value}&name=restaurant`,
          [],
          {
            headers: {
              Authorization: `Bearer ${localStorage.getItem("token")}`
            }
          }
        )
        .then(response => {
          commit("setNavbarId");
          console.log(response);
        })
        .catch(error => {
          console.log(error);
        });
    },
    loadFavouriteObjects({ commit }) {
      axios
        .get(`${process.env.VUE_APP_URL}/api/Favourite/allforuser`, {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`
          }
        })
        .then(response => {
          commit("setFavouriteObjects", response.data);
        })
        .catch(error => {
          console.log(error);
        });
    },
    resetDeleteSnackbar({ commit }) {
      commit("resetDeleteSnackbar");
    },
    deleteAttraction({ commit, dispatch }, id) {
      axios
        .delete(`${process.env.VUE_APP_URL}/api/Attraction/${id}`, {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`
          }
        })
        .then(response => {
          commit("setDeleteSnackbar");
          dispatch("loadLimitedAttractions");
          console.log(response);
        })
        .catch(error => {
          console.log(error);
        });
    },
    deleteHotel({ commit, dispatch }, id) {
      axios
        .delete(`${process.env.VUE_APP_URL}/api/Hotel/${id}`, {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`
          }
        })
        .then(response => {
          commit("setDeleteSnackbar");
          dispatch("loadLimitedHotels");
          console.log(response);
        })
        .catch(error => {
          console.log(error);
        });
    },
    deleteRestaurant({ commit, dispatch }, id) {
      axios
        .delete(`${process.env.VUE_APP_URL}/api/Restaurant/${id}`, {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`
          }
        })
        .then(response => {
          commit("setDeleteSnackbar");
          dispatch("loadLimitedRestaurants");
          console.log(response);
        })
        .catch(error => {
          console.log(error);
        });
    },
    resetEditSnackbar({ commit }) {
      commit("resetEditSnackbar");
    },
    editAttraction({ commit, dispatch }, value) {
      console.log(value);
      axios
        .put(
          `${process.env.VUE_APP_URL}/api/Attraction/${value.monumentId}`,
          value,
          {
            headers: {
              Authorization: `Bearer ${localStorage.getItem("token")}`
            }
          }
        )
        .then(response => {
          console.log(response);
          commit("setEditSnackbar");
          dispatch("loadLimitedAttractions");
        })
        .catch(error => {
          console.log(error);
        });
    },
    editHotel({ commit, dispatch }, value) {
      axios
        .put(`${process.env.VUE_APP_URL}/api/Hotel/${value.hotelId}`, value, {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`
          }
        })
        .then(response => {
          console.log(response);
          commit("setEditSnackbar");
          dispatch("loadLimitedHotels");
        })
        .catch(error => {
          console.log(error);
        });
    },
    editRestaurant({ commit, dispatch }, value) {
      axios
        .put(
          `${process.env.VUE_APP_URL}/api/Restaurant/${value.restaurantId}`,
          value,
          {
            headers: {
              Authorization: `Bearer ${localStorage.getItem("token")}`
            }
          }
        )
        .then(response => {
          console.log(response);
          commit("setEditSnackbar");
          dispatch("loadLimitedRestaurants");
        })
        .catch(error => {
          console.log(error);
        });
    }
  }
});
